FROM golang:1.13.7-alpine3.10 AS build

ARG HUGO_VERSION="0.65.3"
ARG NOW_VERSION="17.0.4"
ARG HUGO_FLAGS="-tags extended"

# --- Build Hugo extended
ENV GO111MODULE=on

WORKDIR /go/src/github.com/gohugoio/hugo
RUN apk add --no-cache git build-base
RUN wget -O- https://github.com/gohugoio/hugo/archive/v${HUGO_VERSION}.tar.gz | tar -xz --strip=1
RUN go install -v -ldflags '-s -w' ${HUGO_FLAGS}

# ---
FROM mhart/alpine-node:12

# Install hugo
COPY --from=build /go/bin/hugo /usr/local/bin
RUN hugo version

# Install now
RUN npm install -g --silent --unsafe-perm now@${NOW_VERSION}
RUN now --version
