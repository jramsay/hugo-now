# hugo-now

Small Alpine container with [Hugo](http://gohugo.io/) (extended) and [Zeit](https://zeit.co/).

## Usage

```yml
image: registry.gitlab.com/jramsay/hugo-now:0.58

stages:
  - review
  - production
```

## Build

```bash
$ docker build --tag "registry.gitlab.com/jramsay/hugo-now:0.58" . && docker push "registry.gitlab.com/jramsay/hugo-now:0.58"
```
